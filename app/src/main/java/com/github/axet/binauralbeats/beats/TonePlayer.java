package com.github.axet.binauralbeats.beats;

import android.content.Context;

import java.util.ArrayList;

public class TonePlayer extends VoicesPlayer {
    public TonePlayer(Context context) {
        super(context);
    }

    public State createBase(float freq, float base) {
        ArrayList<BinauralBeatVoice> voices = new ArrayList<>();
        voices.add(new BinauralBeatVoice(freq, freq, BeatsPlayer.DEFAULT_VOLUME, base));
        return new State(voices);
    }

    public void setBase(float freq, float base) {
        State state = createBase(freq, base);
        play(state);
    }

    public void toneStart(float freq, float base) {
        State state = createBase(freq, base);
        state.setFadeStart(0f, buf.hz, FADE_PROGRAM);
        state.setInfinite();
        play(state);
    }

    public void tonePulse(float freq, float base) {
        State state = createBase(freq, base);
        toneEnd(state);
    }

    public void toneEnd() {
        State state = new State(this.state);
        toneEnd(state);
    }

    public void toneEnd(State state) {
        state.samplesNum = 0;
        state.setDur(buf.hz, 1000);
        state.setFadeStart(0f, 0, 0);
        state.setFadeEnd(0f, buf.hz, FADE_PROGRAM);
        play(state);
    }
}
