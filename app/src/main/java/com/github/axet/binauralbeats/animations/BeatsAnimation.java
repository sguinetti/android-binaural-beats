package com.github.axet.binauralbeats.animations;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Handler;
import android.view.View;
import android.view.animation.Transformation;
import android.widget.ListView;

import com.github.axet.androidlibrary.animations.MarginAnimation;
import com.github.axet.binauralbeats.R;

public class BeatsAnimation extends MarginAnimation {
    Handler handler = new Handler();

    ListView list;

    View convertView;
    View bottom_ctr;

    View button_loop;
    View button_minus;
    View button_plus;
    View button_play;
    View button_stop;

    View bottom_s;
    View compact_s;

    boolean partial;
    boolean playing;

    // true if this animation was started simultaneously with expand animation.
    boolean collapse_multi = false;

    // if we have two concurrent animations on the same listview
    // the only one 'expand' should have control of showChild function.
    static BeatsAnimation atomicExpander;

    public static void apply(final ListView list, final View v, final boolean expand, boolean animate, final boolean playing) {
        apply(new LateCreator() {
            @Override
            public MarginAnimation create() {
                BeatsAnimation a = new BeatsAnimation(list, v, expand, playing);
                if (expand)
                    atomicExpander = a;
                return a;
            }
        }, v, expand, animate);
    }

    public BeatsAnimation(ListView list, View v, boolean expand, boolean playing) {
        super(v.findViewById(R.id.recording_player), expand);

        this.convertView = v;
        this.list = list;
        this.playing = playing;

        bottom_ctr = v.findViewById(R.id.beats_controls);
        button_minus = v.findViewById(R.id.button_minus);
        button_plus = v.findViewById(R.id.button_plus);
        button_loop = v.findViewById(R.id.button_loop);
        button_play = bottom_ctr.findViewById(R.id.beats_player_play);
        button_stop = bottom_ctr.findViewById(R.id.beats_player_stop);
        bottom_s = v.findViewById(R.id.beats_expand);
        compact_s = v.findViewById(R.id.beats_compact);
    }

    public void init() {
        super.init();

        button_plus.setVisibility(View.VISIBLE);
        button_minus.setVisibility(View.VISIBLE);
        button_play.setVisibility(playing ? View.VISIBLE : View.INVISIBLE);
        button_stop.setVisibility(playing ? View.VISIBLE : View.INVISIBLE);
        button_loop.setVisibility(View.VISIBLE);
        bottom_s.setVisibility(expand ? View.INVISIBLE : View.VISIBLE);
        compact_s.setVisibility(expand ? View.VISIBLE : View.INVISIBLE);

        {
            final int paddedTop = list.getListPaddingTop();
            final int paddedBottom = list.getHeight() - list.getListPaddingTop() - list.getListPaddingBottom();

            partial = false;

            partial |= convertView.getTop() < paddedTop;
            partial |= convertView.getBottom() > paddedBottom;
        }
    }

    @Override
    public void calc(final float i, Transformation t) {
        super.calc(i, t);

        float ii = expand ? i : 1 - i;
        float k = expand ? 1 - i : i;

        if (Build.VERSION.SDK_INT >= 11) {
            compact_s.setRotation(180 * ii);
            button_plus.setAlpha(ii);
            button_minus.setAlpha(ii);
            if (playing) {
                button_play.setAlpha(k);
                button_stop.setAlpha(k);
            } else {
                button_loop.setAlpha(ii);
            }
            bottom_s.setRotation(-180 + 180 * ii);
        }

        // ViewGroup will crash on null pointer without this post pone.
        // seems like some views are removed by RecyvingView when they
        // gone off screen.
        if (Build.VERSION.SDK_INT >= 19) {
            collapse_multi |= !expand && atomicExpander != null && !atomicExpander.hasEnded();
            if (collapse_multi) {
                // do not showChild;
            } else {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        showChild(i);
                    }
                });
            }
        }
    }

    @TargetApi(19)
    void showChild(float i) {
        final int paddedTop = list.getListPaddingTop();
        final int paddedBottom = list.getHeight() - list.getListPaddingTop() - list.getListPaddingBottom();

        if (convertView.getTop() < paddedTop) {
            int off = convertView.getTop() - paddedTop;
            if (partial)
                off = (int) (off * i);
            list.scrollListBy(off);
        }

        if (convertView.getBottom() > paddedBottom) {
            int off = convertView.getBottom() - paddedBottom;
            if (partial)
                off = (int) (off * i);
            list.scrollListBy(off);
        }
    }

    @Override
    public void restore() {
        super.restore();
        if (Build.VERSION.SDK_INT >= 11) {
            button_plus.setAlpha(1);
            button_minus.setAlpha(1);
            bottom_s.setRotation(0);
            compact_s.setRotation(0);
        }
    }

    @Override
    public void end() {
        super.end();
        if (expand) {
            button_plus.setVisibility(View.VISIBLE);
            button_minus.setVisibility(View.VISIBLE);
            button_play.setVisibility(View.INVISIBLE);
            button_stop.setVisibility(View.INVISIBLE);
            button_loop.setVisibility(View.VISIBLE);
        } else {
            button_plus.setVisibility(View.INVISIBLE);
            button_minus.setVisibility(View.INVISIBLE);
            button_play.setVisibility(playing ? View.VISIBLE : View.INVISIBLE);
            button_stop.setVisibility(playing ? View.VISIBLE : View.INVISIBLE);
            button_loop.setVisibility(playing ? View.VISIBLE : View.INVISIBLE);
        }
        bottom_s.setVisibility(expand ? View.VISIBLE : View.INVISIBLE);
        compact_s.setVisibility(expand ? View.INVISIBLE : View.VISIBLE);
        view.setVisibility(expand ? View.VISIBLE : View.GONE);
    }
}