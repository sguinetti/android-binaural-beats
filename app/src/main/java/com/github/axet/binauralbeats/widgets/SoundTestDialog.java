package com.github.axet.binauralbeats.widgets;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.github.axet.binauralbeats.R;
import com.github.axet.binauralbeats.app.MainApplication;
import com.github.axet.binauralbeats.app.Sound;
import com.github.axet.binauralbeats.beats.BeatsPlayer;
import com.github.axet.binauralbeats.beats.BinauralBeatVoice;
import com.github.axet.binauralbeats.beats.Period;
import com.github.axet.binauralbeats.beats.Program;
import com.github.axet.binauralbeats.beats.SoundLoop;
import com.github.axet.binauralbeats.beats.VoicesPlayer;
import com.github.axet.binauralbeats.beats.WhiteNoise;

public class SoundTestDialog extends DialogFragment {
    TextView text;
    View white;
    View left;
    View right;

    WhiteNoise whiteNoise;
    VoicesPlayer player;
    Program pr;

    boolean l = false;
    boolean r = false;

    float vol;
    float bg;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        vol = Sound.log1(MainApplication.getFlatVcVol(getContext()));
        bg = Sound.log1(MainApplication.getFlatBgVol(getContext()));

        whiteNoise = new WhiteNoise(getContext());

        pr = new Program(getString(R.string.group_mediation));
        Period p = new Period(0, SoundLoop.NONE, BeatsPlayer.BG_VOLUME_RATIO);
        p.addVoice(new BinauralBeatVoice(10, 10, 0.65f, 520));
        pr.addPeriod(p);

        player = new VoicesPlayer(getContext());
        player.start();
        player.setVolume(0);
        player.playVoices(p.getVoices());
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getContext());

        View view = inflater.inflate(R.layout.soundtest, null, false);

        white = view.findViewById(R.id.test_white);
        white.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (whiteNoise.isPlaying())
                    whiteNoise.stop();
                else
                    whiteNoise.start();
                update();
            }
        });
        left = view.findViewById(R.id.test_left);
        left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                l = !l;
                update();
            }
        });
        right = view.findViewById(R.id.test_right);
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                r = !r;
                update();
            }
        });
        text = (TextView) view.findViewById(R.id.test_text);

        update();

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(view);
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        final AlertDialog dialog = builder.create();
        return dialog;
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Activity a = getActivity();
        if (a instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) a).onDismiss(dialog);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        close();
    }

    void update() {
        ShapeDrawable drawable;

        int GRAY = Color.GRAY;

        whiteNoise.setVolume(bg * BeatsPlayer.BG_VOLUME_RATIO);
        drawable = new ShapeDrawable(new OvalShape());
        if (whiteNoise.isPlaying())
            drawable.getPaint().setColor(GRAY);
        else
            drawable.getPaint().setColor(Color.TRANSPARENT);
        white.setBackgroundDrawable(drawable);

        drawable = new ShapeDrawable(new OvalShape());
        if (l)
            drawable.getPaint().setColor(GRAY);
        else
            drawable.getPaint().setColor(Color.TRANSPARENT);
        left.setBackgroundDrawable(drawable);

        drawable = new ShapeDrawable(new OvalShape());
        if (r)
            drawable.getPaint().setColor(GRAY);
        else
            drawable.getPaint().setColor(Color.TRANSPARENT);
        right.setBackgroundDrawable(drawable);

        text.setVisibility((l && r) ? View.VISIBLE : View.INVISIBLE);

        player.setVolume(l ? vol : 0f, r ? vol : 0f);
    }

    void close() {
        if (whiteNoise != null) {
            whiteNoise.setVolume(0);
            whiteNoise.close();
            whiteNoise = null;
        }
        if (player != null) {
            player.setVolume(0);
            player.shutdown();
            player = null;
        }
    }
}
